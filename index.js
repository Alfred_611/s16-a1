console.log("For Loop");


let number = Number(prompt("Please give me a number"));

console.log("The number is divisible by 10. Skipping the number.");
	
for(number; number >= 50; number--) {
	if(number % 10 == 0){
		console.log(number);
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	}

	if(number % 5 == 0){
		console.log(number);
	}

	console.log(number);
	
	if(number <= 50){
		console.log(number);
		console.log("The current value is at 50. Terminate the loop.")
		break;
	}
}


console.log("Iterating the consonants of a string");

let word = "supercalifragilisticexpialidocious";
let emptySpace = ""
//word.length = 34
console.log(word);
for(let a = 0; a <word.length; a++){
	if(
		word[a] == "a" ||
		word[a] == "e" ||
		word[a] == "i" ||
		word[a] == "o" ||
		word[a] == "u"
	){
		//console.log(word[a]);
	} else {
		emptySpace += word[a];
	}
}
console.log(emptySpace);
